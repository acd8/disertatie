

	// A combination of Neuro.js with Pixi.js and P2.js to interact with graphics and gravity forces
	//some functions is implemented from Neuro.js library or created based on Neuro.js library's code
	//some export modules is initialised based on Neuro.js library ( - Copyright (c) Sindre Sorhus and Jan Huermann) 
//--------------------------------------------------------------------------------------------------
	//for training with keyboard
	var isKeyboardTrain=true;

	//animate wheel for train
	function wheelControl(rotation){
		   
	    $('#wheel').css({
        "-moz-transform": "rotate(" + rotation/Math.PI + "deg)",
        "-webkit-transform": "rotate(" + rotation/Math.PI + "deg)",
        "-o-transform": "rotate(" + rotation/Math.PI + "deg)",
        "-ms-transform": "rotate(" + rotation/Math.PI + "deg)",
        
    });
	}
//--------------------------------------------------------------------------------------------------
	//bootstrap modules for exporting functions to agent to agent (export easily class)
    //link from here 
    //https://getbootstrap.com/docs/4.0/getting-started/webpack/
(function(modules) { 

 	var installedModules = {};
    function getFunctions(moduleId) {

 		// Check if module is in cache
 		if(installedModules[moduleId])
 			return installedModules[moduleId].exports;

 	
 		var module = installedModules[moduleId] = {
 			exports: {},
 			id: moduleId,
 			loaded: false
 		};

 		
 		modules[moduleId].call(module.exports, module, module.exports, getFunctions);

 	
 		module.loaded = true;

 		return module.exports;
 	}
	getFunctions.m = modules;
	getFunctions.c = installedModules;
	getFunctions.p = "";
 	return getFunctions(0);
 })
 ([
 function(module, exports, getFunctions) {

//--------------------------------------------------------------------------------------------------
	//Initialise constructor of classes car and app from webkit bootstap

	var app = getFunctions(1); //load App class
	var car = getFunctions(3); //load Car class

	//Initialize graphics using Pixi.js functions
	function boot() {
	    this.world = new app.world();
	    this.renderer = new app.renderer(this.world, document.getElementById("container"));

	    this.world.init(this.renderer)
	    this.world.populate(1)

	    this.dispatcher = new app.dispatcher(this.renderer, this.world);
	    this.dispatcher.begin();

	

	    return this.dispatcher;
	};
//--------------------------------------------------------------------------------------------------
	//save trained data
	function saveAs(dv, name) {
	    var a;
	    if (typeof window.downloadAnchor == 'undefined') {
	        a = window.downloadAnchor = document.createElement("a");
	        a.style = "display: none";
	        document.body.appendChild(a);
	    } else {
	        a = window.downloadAnchor
	    }

	    var blob = new Blob([dv], { type: 'application/octet-binary' }),
	        tmpURL = window.URL.createObjectURL(blob);

	    a.href = tmpURL;
	    a.download = name;
	    a.click();

	    window.URL.revokeObjectURL(tmpURL);
	    a.href = "";
	}
//--------------------------------------------------------------------------------------------------
	//download data from server
	function downloadBrain(n) {
		var buf = window.gcd.world.agents[n].brain.export()
		saveAs(new DataView(buf), 'dataset.bin')
	}

	function saveEnv() {
	    saveAs(new DataView(window.gcd.world.export()), 'dataset_saved.bin')
	}
//--------------------------------------------------------------------------------------------------
//Read data from local files
	function readBrain(e) {
	    var input = e.target;
	    var reader = new FileReader();
	    reader.onload = function(){
	        var buffer = reader.result
	        var imported = window.neurojs.NetOnDisk.readMultiPart(buffer)

	        for (var i = 0; i <  window.gcd.world.agents.length; i++) {
	            window.gcd.world.agents[i].brain.algorithm.actor.set(imported.actor.clone())
	            window.gcd.world.agents[i].brain.algorithm.critic.set(imported.critic)
	             window.gcd.world.agents[i].car.brain.learning = true
	        }
	    };

	    reader.readAsArrayBuffer(input.files[0]);
		console.log("brain read!");
		$("#stare_date").text("Antrenate");
	}
function readWorld(e) {
	    var input = e.target;

	    var reader = new FileReader();
	    reader.onload = function(){
	        var buffer = reader.result
	        window.gcd.world.import(buffer)
	    };

	    reader.readAsArrayBuffer(input.files[0]);
	}
	function stats() {
	    var agent = window.gcd.world.agents[0];
	    // window.infopanel.age.innerText = Math.floor(window.gcd.world.age) + '';
	}


//--------------------------------------------------------------------------------------------------
	//initialize saved or load data on graphics
	window.gcd = boot();
	window.downloadBrain = downloadBrain;
	window.saveEnv = saveEnv
	window.readWorld = readWorld
	window.updateIfLearning = function (value) {
	    for (var i = 0; i <  window.gcd.world.agents.length; i++) {
	        window.gcd.world.agents[i].brain.learning = value
	    }

	    window.gcd.world.plotRewardOnly = !value
	};

	window.readBrain = readBrain;

	setInterval(stats, 100);

},


//--------------------------------------------------------------------------------------------------
//initialize functions on variables
 function(module, exports, getFunctions) {

	module.exports = {
		agent: getFunctions(2),
		car: getFunctions(3),
		dispatcher: getFunctions(7),
		keyboard: getFunctions(8),
		renderer: getFunctions(9),
		world: getFunctions(10)
	};

},
//--------------------------------------------------------------------------------------------------
//initialize class Agent
 function(module, exports, getFunctions) {
	var car = getFunctions(3);
	//read data from options and graphics data
	function agent(opt, world) {
	    this.car = new car(world, {})
	    this.options = opt

	    this.world = world
	    this.frequency = 20
	    this.reward = 0
	    this.loaded = false

	    this.loss = 0
	    this.timer = 0
	    this.timerFrequency = 60 / this.frequency

	    if (this.options.dynamicallyLoaded !== true) {
	    	this.init(world.brains.actor.newConfiguration(), null)
	    }
	    
	};	
	//set ddpg algorithm from Neuro.Js, just initialized from Neuro.js
	agent.prototype.init = function (actor, critic) {
	    var actions = 2
	    var temporal = 1
	    var states = this.car.sensors.dimensions

	    var input = window.neurojs.Agent.getInputDimension(states, actions, temporal)

	    this.brain = new window.neurojs.Agent({
	    	//set data for ddpg algorithm
	        actor: actor, //initialize actor
	        critic: critic, //initialize critic states for algorithm

	        states: states, //initialize states
	        actions: actions, //initialize actions

	        algorithm: 'ddpg',

	        temporalWindow: temporal, 

	        discount: 0.97, 

	        experience: 75e3, 
	        learningPerTick: 40, 
	        startLearningAt: 900,

	        theta: 0.05, // progressive copy

	        alpha: 0.1 // advantage learning

	    })

	    this.world.brains.shared.add('critic', this.brain.algorithm.critic) //set agent brain
	    this.actions = actions
	    this.car.addToWorld()
		this.loaded = true
	};

//--------------------------------------------------------------------------------------------------
	//checking if everything works and reward is working alongside with speed	

	agent.prototype.step = function (dt) {
		if (!this.loaded) {
			return 
		}

	    this.timer++

	    if (this.timer % this.timerFrequency === 0) {
	        this.car.update()

	        var vel = this.car.speed.local
	        var speed = this.car.speed.velocity
	        this.reward = Math.pow(vel[1], 2) - 0.10 * Math.pow(vel[0], 2) - this.car.contact * 10 - this.car.impact * 20 //calculate reward based on speed and velocity

	        if (Math.abs(speed) < 1e-2) { //punish agent if speed is slow
	            this.reward -= 1.0 
	        }

	        this.loss = this.brain.learn(this.reward)
	        this.action = this.brain.policy(this.car.sensors.data)
	        
	        this.car.impact = 0
	        this.car.step()
	    }
	    //supervized or unsupervised learning
	    if (this.action) {
	    	if(isKeyboardTrain){
	        this.car.handle(this.action[0], this.action[1])

	    }
	    }

	    return this.timer % this.timerFrequency === 0
	};

	agent.prototype.draw = function (context) {
	};

	module.exports = agent;

},

//--------------------------------------------------------------------------------------------------
//Initialize car body
 function(module, exports, getFunctions) {

	var color = getFunctions(4),
	    sensors = getFunctions(5),
	    tc = getFunctions(6);

	class Car {

	    constructor(world, opt) {
	        this.maxSteer = Math.PI / 7 //set max for steer
	        this.maxEngineForce = 10 //force acceleration for engine
	        this.maxBrakeForce = 5 //braking capacity
	        this.maxBackwardForce = 2  //for when car detect a object
	        this.linearDamping = 0.5 // for car size

	        this.contact = 0
	        this.impact = 0
	        //those values is implicit set to 0 because car is randomly initialised on graphics
	        this.world = world

	        this.init()
	    }
	    //Car constructor
	    init() {
	        this.carRealBody()
	        //initialisze sensors on car using Pixi.js and P2.js 
	        this.sensors = Car.Sensors.build(this)
	        this.speed = this.sensors.getByType("speed")[0]
	    }

	    carRealBody() {
	        // Create a dynamic body for the chassis
	        this.chassisBody = new p2.Body({
	            mass: 1,
	            damping: 0.2,
	            angularDamping: 0.3,
	            ccdSpeedThreshold: 0,
	            ccdIterations: 40
	        });
	        //based on implicit Pixi2.js creating objecs
	        this.body_1 = {}
			this.chassisBody.color = 0xff001f;
	        this.chassisBody.car = true;
	        this.chassisBody.damping = this.linearDamping;



	        var boxShape = new p2.Box({ width: 0.5, height: 1 });
	        boxShape.entity = Car.ShapeEntity

	        this.chassisBody.addShape(boxShape);
	        this.chassisBody.gl_create = (function (sprite, r) {
	            this.overlay = new PIXI.Graphics();
	            this.overlay.visible = true;
				 sprite.addChild(this.overlay);
	            var body_1 = new PIXI.Graphics()
	            sprite.addChild(body_1)

	            var w = 0.12, h = 0.22
	            var space = 0.07
	            var col = "#ffffff"
	                col = "#ffffff"
	            var alpha = 1, alphal = 1

	            var tl = new PIXI.Graphics()
	            var tr = new PIXI.Graphics()

	            tl.beginFill(col, alpha)
	            tl.position.x = -0.25
	            tl.position.y = 0.5 - h / 2 - space
	            tl.drawRect(-w / 2, -h / 2, w, h)
	            tl.endFill()

	            tr.beginFill(col, alpha)
	            tr.position.x = 0.25
	            tr.position.y = 0.5 - h / 2 - space
	            tr.drawRect(-w / 2, -h / 2, w, h)
	            tr.endFill()

	            this.body_1.topLeft = tl
	            this.body_1.topRight = tr
	        }).bind(this); 

	        // Create the vehicle
	        this.vehicle = new p2.TopDownVehicle(this.chassisBody);

	        // Add one front wheel and one back wheel - we don't actually need four :)
	        this.frontWheel = this.vehicle.addWheel({
	            localPosition: [0, 0.5] // front
	        });
	        this.frontWheel.setSideFriction(50);

	        // Back wheel
	        this.backWheel = this.vehicle.addWheel({
	            localPosition: [0, -0.5] // back
	        })
	        this.backWheel.setSideFriction(45) // Less side friction on back wheel makes it easier to drift
	    }

	    update() {
	    	//update sensors
	        this.sensors.update()
	    }

	    step() {
	    	// car steps per miniseconds
	        this.draw()
	    }

	    draw() {
	    	//draw car per miniseconds and clear object
	        if (this.overlay.visible !== true) {
	            return
	        }

	        this.overlay.clear() 
	        this.sensors.draw(this.overlay)
	    }

	    //Main function for car's control (engine)
	    handle(throttle, steer) {
	        this.frontWheel.steerValue = this.maxSteer * steer

	        // Engine force forward
	        var force = throttle * this.maxEngineForce
	        if (force < 0) {
	            if (this.backWheel.getSpeed() > 0.1) {
	                this.backWheel.setBrakeForce(-throttle * this.maxBrakeForce)
	                this.backWheel.engineForce = 0.0
	         //allow interface to be updated in realtime with images
	         var logo = document.getElementById('pedals');
			logo.src = "pedals/4.png";       
	         var logo = document.getElementById('pedals');
			logo.src = "pedals/3.png";
		
	            }
	        else {
	        this.backWheel.setBrakeForce(0)
	        this.backWheel.engineForce = throttle * this.maxBackwardForce
	        var logo = document.getElementById('pedals');
			logo.src = "pedals/1.png";
	            }
	        }
	        else {
	            this.backWheel.setBrakeForce(0)
	            this.backWheel.engineForce = force
	                var logo = document.getElementById('pedals');
			logo.src = "pedals/2.png";
	        }

	        this.body_1.topLeft.rotation = this.frontWheel.steerValue * 0.7071067812
	        this.body_1.topRight.rotation = this.frontWheel.steerValue * 0.7071067812
	        wheelControl(this.frontWheel.steerValue*180);
	    }
	    //initialize keyboard for control car
	    handleKeyboard(k) {
	        this.handle((k.getN(38) - k.getN(40)), (k.getN(39) - k.getN(37)))
	       //  console.log("pressed");

	          if(k.getD(38)==1){
		          	// console.log("pressed 38 up val=" + k.getD(38));

	          }

	          if(k.getD(39)==1){
	          	// console.log("pressed 39 right");
	       
	          }

	          if(k.getD(40)==1){
	          	// console.log("pressed 40 down");
	          }

	          if(k.getD(37)==1){
	          	// console.log("pressed 47 left");
	          }
	          //Set supervised or unsupervised mode
	           if(k.getD(77)==1){
	           	console.log("invatare supervizata");
				  isKeyboardTrain=false;
				  $("[name='my-checkbox']").bootstrapSwitch('state',true);
	          }
	           if(k.getD(65)==1){
	          	console.log("invatare nesupervizata");
				  isKeyboardTrain=true;
				  $("[name='my-checkbox']").bootstrapSwitch('state',false);
	          }
	        if (k.getD(83) === 1) {
	            this.overlay.visible = !this.overlay.visible;
	        }
	    }

	    //adding car to graphics
	    addToWorld() {
	        this.chassisBody.position[0] = (Math.random() - .5) * this.world.size.w
	        this.chassisBody.position[1] = (Math.random() - .5) * this.world.size.h
	        this.chassisBody.angle = (Math.random() * 2.0 - 1.0) * Math.PI
	        //add body in Graphics
	        this.world.p2.addBody(this.chassisBody)
	        this.vehicle.addToWorld(this.world.p2)
	        //begin contact with graphics's objects
	        this.world.p2.on("beginContact", (event) => {
	            if ((event.bodyA === this.chassisBody || event.bodyB === this.chassisBody)) {
	                this.contact++;
	            }
	        });
	        //end contact, car is free
	        this.world.p2.on("endContact", (event) => {
	            if ((event.bodyA === this.chassisBody || event.bodyB === this.chassisBody)) {
				   this.contact--;
				   document.getElementById('coliziune').style.background="#b7b7b7";
				   document.getElementById('coliziune').innerHTML="Coliziune: Liber";
	            }
	        })

	        this.world.p2.on("impact", (event) => {
	            if ((event.bodyA === this.chassisBody || event.bodyB === this.chassisBody)) {
					this.impact = Math.sqrt(Math.pow(this.chassisBody.velocity[0], 2) + Math.pow(this.chassisBody.velocity[1], 2))
					document.getElementById('coliziune').style.background="#c10000";
					document.getElementById('coliziune').innerHTML="Colizinune: Zid";
						// $('#coliziune').css("color","red");
	            }
	        })

	    }

	}
	//car endity and initialise senzors
	Car.ShapeEntity = 2

	Car.Sensors = (() => {
	    var d = 0.05 //sensor width
	    var r = -0.25 + d, l = +0.25 - d //sensor range
	    var b = -0.50 + d, t = +0.50 - d //sensor distance
	    //initialise senzors
	    return sensors.DetectionSensor.compile([
	   //distance sensors
	        { type: 'distance', angle: -45, length: 5, start: [ r, t ] },
	        { type: 'distance', angle: -30, length: 5, start: [ 0, t ] },
	        { type: 'distance', angle: -15, length: 5, start: [ 0, t ] },
	        { type: 'distance', angle: +00, length: 5, start: [ 0, t ] },
	        { type: 'distance', angle: +15, length: 5, start: [ 0, t ] },
	        { type: 'distance', angle: +30, length: 5, start: [ 0, t ] },
	        { type: 'distance', angle: +45, length: 5, start: [ l, t ]  },

	        { type: 'distance', angle: +135, length: 5, start: [ l, b ]  },
	        { type: 'distance', angle: +165, length: 5, start: [ 0, b ]  },
	        { type: 'distance', angle: -180, length: 5, start: [ 0, b ]  },
	        { type: 'distance', angle: -165, length: 5, start: [ 0, b ]  },
	        { type: 'distance', angle: -135, length: 5, start: [ r, b ]  },

	        { type: 'distance', angle: -10, length: 10, start: [ 0, t ]  },
	        { type: 'distance', angle: -03, length: 10, start: [ 0, t ]  },
	        { type: 'distance', angle: +00, length: 10, start: [ 0, t ]  },
	        { type: 'distance', angle: +03, length: 10, start: [ 0, t ]  },
	        { type: 'distance', angle: +10, length: 10, start: [ 0, t ]  },

	        { type: 'distance', angle: +60, length: 5, start: [ l, 0 ]  },
	        { type: 'distance', angle: +90, length: 5, start: [ l, 0 ]  },
	        { type: 'distance', angle: +120, length: 5, start: [ l, 0 ]  },

	        { type: 'distance', angle: -60, length: 5, start: [ r, 0 ]  },
	        { type: 'distance', angle: -90, length: 5, start: [ r, 0 ]  },
	        { type: 'distance', angle: -120, length: 5, start: [ r, 0 ]  },
	        //speed sensor
	        { type: 'speed' },

	    ])
	})()
	//return to car function
	module.exports = Car;

},

 function(module, exports) {

 },
//--------------------------------------------------------------------------------------------------
//Create sensors
function(module, exports, getFunctions) {
	//get variables from car and colors
	var color = getFunctions(4);
	var car = getFunctions(3)

	class Sensor {

	}
//--------------------------------------------------------------------------------------------------	
	//initialize distance sensors
	class DistanceSensor extends Sensor {
		//setting constructor and initialize constructor details on Pixi2.js
	    constructor(car, opt) {
	        super()
	        this.type = "distance"
	        this.car = car
	        this.angle = opt.angle / 180 * Math.PI
	        this.length = opt.length || 10
	        this.absolute = opt.absolute || false

	        this.direction = [ Math.sin(this.angle), Math.cos(this.angle) ]
	        this.start = opt.start || [ 0, 0.1 ]

	        this.localNormal = p2.vec2.create()
	        this.globalRay = p2.vec2.create()

	        this.ray = new p2.Ray({
	            mode: p2.Ray.CLOSEST,
	            direction: this.direction,
	            length: this.length,
	            checkCollisionResponse: false,
	            skipBackfaces: true
	        })
	        //set implicit length
	        this.setLength(this.length);

	        this.castedResult = new p2.RaycastResult()
	        this.hit = false
	        this.distance = 0.0
	        this.entity = 0

	        this.data = new Float64Array(DistanceSensor.dimensions)
	    }

	    setLength(v) {
	    	//initialise directions
	        this.length = v
	        this.ray.length = this.length
	        this.end = [ this.start[0] + this.direction[0] * this.length, this.start[1] + this.direction[1] * this.length ]
	        this.rayVector = [ this.end[0] - this.start[0], this.end[1] - this.start[1] ]
	    }
	    //update graphics and set sensors over Car
	    update() {
	        var vehicleBody = this.car.chassisBody;
	        if (vehicleBody.world === null) {
	            this.data.fill(0.0)
	            return
	        }

	        vehicleBody.toWorldFrame(this.ray.from, this.start);
	        vehicleBody.toWorldFrame(this.ray.to, this.end);
	        
	        this.ray.update();
	        this.castedResult.reset();

	        vehicleBody.world.raycast(this.castedResult, this.ray);

	        //if sensoris detect an object, then send data to car
	        if (this.hit = this.castedResult.hasHit()) {
	            this.distance = this.castedResult.fraction
	            this.entity = this.castedResult.shape.entity
				 var angle = Math.atan2( this.castedResult.normal[1], this.castedResult.normal[0] ) - Math.atan2( this.globalRay[1], this.globalRay[0] ) // = Math.atan2( this.localNormal[1], this.localNormal[0] ) - Math.atan2( this.rayVector[1], this.rayVector[0] )    
	            if (angle > Math.PI / 2) angle = Math.PI - angle
	            if (angle < -Math.PI / 2) angle = Math.PI + angle
	            this.data[0] = 1.0 - this.distance
	            // this.data[1] = angle
	            this.data[1] = this.entity === car.ShapeEntity ? 1.0 : 0.0 // is car?
	            this.data[2] = 1.0 // hit?
	        } 

	        else {
	            this.data.fill(0.0)
	        }
	    }

	    draw(g) {
	        var dist = this.hit ? this.distance : 1.0
	        var c = "#ffffff"
	        g.lineStyle(this.highlighted ? 0.04 : 0.01, c, 0.5)
	        g.moveTo(this.start[0], this.start[1]);
	        g.lineTo(this.start[0] + this.direction[0] * this.length * dist, this.start[1] + this.direction[1] * this.length * dist);
	    }

	}
//--------------------------------------------------------------------------------------------------
//this sensor detect velocity, acceleration and send to trained data
	class SpeedSensor extends Sensor {
		//initialize sensor on car
	    constructor(car, opt) {
	        super()
	        this.type = "speed"
	        this.car = car
	        this.local = p2.vec2.create()
	        this.data = new Float64Array(SpeedSensor.dimensions)
	    }
	    //update car on graphics
	    update() {
	        this.car.chassisBody.vectorToLocalFrame(this.local, this.car.chassisBody.velocity)
	        this.data[0] = this.velocity = p2.vec2.len(this.car.chassisBody.velocity) * (this.local[1] > 0 ? 1.0 : -1.0)
	        this.data[1] = this.local[1]
	        this.data[2] = this.local[0]
	        var speed=Math.floor(this.velocity*3.6);
	        //show details on Graphics about agent's action
	         document.getElementById('speed-val').innerHTML=speed+" km/h";
	         if(speed<0){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/6.png";
			}
			 else if(speed==0){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/0.png";
			}
			 else if(speed<20){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/1.png";
			}
			 else if(speed<40){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/2.png";
			}
			else if(speed<60){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/3.png";
			}
			else if(speed<80){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/4.png";
			}
			else if(speed<100){
	         var logo = document.getElementById('gearbox');
			logo.src = "gearbox/1.png";
			}
	    }

	    draw(g) {

	    }

	}

//create and generate types of sensors
	const sensorTypes = {
	    "distance": DistanceSensor,
	    "speed": SpeedSensor
	}

	DistanceSensor.dimensions = 3
	SpeedSensor.dimensions = 3
//--------------------------------------------------------------------------------------------------
//create sensor arrays 
	class SensorArray {
		//generate sensor arrays
	    constructor(car, blueprint) {
	        this.sensors = []
	        this.dimensions = blueprint.dimensions
	        this.data = new Float64Array(blueprint.dimensions)
	        //initialize sensors and show on graphics as array
	        for (var i = 0; i < blueprint.list.length; i++) {
	            var opt = blueprint.list[i]
	            this.sensors.push(new sensorTypes[opt.type](car, opt))
	        }
	    }
	    //update sensors on graphics
	    update() {
	        for (var i = 0, k = 0; i < this.sensors.length; k += this.sensors[i].data.length, i++) {
	            this.sensors[i].update()
	            this.data.set(this.sensors[i].data, k)
	        }
	    }
	    //draw sensor arrays
	    draw(g) {
	        for (var i = 0; i < this.sensors.length; i++) {
	            this.sensors[i].draw(g)
	        }
	    }

	    getByType(type) {
	        for (var i = 0, found = []; i < this.sensors.length; i++) {
	            if (this.sensors[i].type === type) {
	                found.push(this.sensors[i])
	            }
	        }
	        return found
	    }

	}
	//create detection sensor
	class DetectionSensor {
		//initialize list of detection sensor
	    constructor(list) {
	        this.list = list
	        this.dimensions = 0

	        for (var i = 0; i < this.list.length; i++) {
	            var opt = this.list[i]
	            this.dimensions += sensorTypes[opt.type].dimensions
	        }
	    }

	    build(car) {
	        return new SensorArray(car, this)
	    }


	    static compile(list) {
	        return new DetectionSensor(list)
	    }

	}

	//export sensors to class
	module.exports = {
	    SensorArray, DetectionSensor
	};

 },
 function(module, exports, getFunctions) {
 },
//--------------------------------------------------------------------------------------------------
//Graphics Pixi2.js initialization
function(module, exports, getFunctions) {

	var requestAnimFrame =  window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) { window.setTimeout(callback, 1000 / 60); };
	var keyboard = getFunctions(8);

	function dispatcher(renderer, world) {
	    this.renderer = renderer;
	    this.world = world;
	    this.running = true;
	    this.interval = false;
	    this.step = 0;
	    //add keyboard events to agent
	    this.keyboard = new keyboard();
	    this.keyboard.subscribe((function (k) {
	        for (var i = 0; i < this.world.agents.length; i++) {
	            this.world.agents[i].car.handleKeyboard(k);
	        }
	        
	    }).bind(this));

	    this.__loop = this.loop.bind(this);
	}
		//initialize loop of graphics
		dispatcher.prototype.loop = function () {
	    if (this.running && !this.interval) {  // start next timer
	        requestAnimFrame(this.__loop);
	    }

	    var dt = 1.0 / 60.0

	    // compute phyiscs
	    this.world.step(dt);
	    this.step++;

	    // draw everything
	    if (!this.interval || this.step % 5 === 0)
	        this.renderer.render();

	};
	//start interval of loop
	dispatcher.prototype.begin = function () {
	    this.running = true;

	    if (this.__interval && !this.interval)
	        clearInterval(this.__interval)

	    if (this.interval)
	        this.__interval = setInterval(this.__loop, 0)
	    else
	        requestAnimFrame(this.__loop)
	};

	

	//runnin lopp
	dispatcher.prototype.stop = function () {
	    this.running = false;
	};

	module.exports = dispatcher;

},
//--------------------------------------------------------------------------------------------------
//Keyboard main function using javascript
function(module, exports) {

	function keyboard() {
	    this.handlers = {};
	    this.subscribers = [];
	    this.states = {};

	    document.onkeydown = this.keydown.bind(this);
	    document.addEventListener('keydown', this.keydown.bind(this), true);
	    document.addEventListener('keyup', this.keyup.bind(this), true);
	}

	keyboard.prototype.keydown = function (e) {
	    this.states[e.keyCode] = this.states[e.keyCode] !== undefined ? this.states[e.keyCode] + 1 : 1;

	    if (this.handlers[e.keyCode]) {
	        this.handlers[e.keyCode](true);
	    }

	    for (var i = 0; i < this.subscribers.length; i++) {
	        this.subscribers[i](this);
	    }
	};

	keyboard.prototype.keyup = function (e) {
	    this.states[e.keyCode] = 0;

	    if (this.handlers[e.keyCode]) {
	        this.handlers[e.keyCode](false);
	    }

	    for (var i = 0; i < this.subscribers.length; i++) {
	        this.subscribers[i](this);
	    }
	};

	keyboard.prototype.listen = function (key, callback) {
	    this.handlers[key] = callback;
	};

	keyboard.prototype.subscribe = function (callback) {
	    this.subscribers.push(callback);
	};

	keyboard.prototype.get = function (key) {
	    if (this.states[key])
	        return this.states[key] > 0;

	    return false;
	};

	keyboard.prototype.getN = function (key) {
	    if (this.states[key])
	        return this.states[key] > 0 ? 1 : 0;

	    return 0;
	};

	keyboard.prototype.getD = function (key) {
	    if (this.states[key])
	        return this.states[key];

	    return 0;
	}

	module.exports = keyboard;

},
//--------------------------------------------------------------------------------------------------
// Creating shapes on map and generating widh P2.js gravitational forces and initialize graphics on Html tag
//functions are initialised from Pixi.js
function(module, exports) {

	
	//add all existent shapes
	function renderer(world, container) {
	    this.world = world;
	    this.world.p2.on("addBody", (e) => {
	        this.add_body(e.body)
	    });

	    this.world.p2.on("removeBody", (e) => {
	        this.remove_body(e.body)
	    })
	    //generate container from HTML
	    if (container) {
	        this.elementContainer = container
	    }

	    else {
	        this.elementContainer = document.createElement("div");
	        this.elementContainer.style.width = "100%";

	        document.body.appendChild(this.elementContainer)
	    }

	    this.pixelRatio = window.devicePixelRatio || 1;

	    this.pixi = new PIXI.autoDetectRenderer(0, 0, {
	        antialias: true,
	        resolution: this.pixelRatio,
	        transparent: true
	    }, false);
	 
	    //create PIXI container
	    this.stage = new PIXI.Container()
	    this.container = new PIXI.DisplayObjectContainer()
	  
	    this.stage.addChild(this.container)
	    //set width and height
	    this.drawPoints = []
	    this.pixi.view.style.width = "100%";
	    this.pixi.view.style.height = "544px";
	
	    this.elementContainer.appendChild(this.pixi.view);

	    this.bodies = [];
	    this.viewport = { scale: 35, center: [0,0], width: 0, height: 0 };

	    window.addEventListener('resize', this.events.resize.bind(this), false);
	    this.adjustBounds();

	    this.drawingGraphic = new PIXI.Graphics()
	    this.container.addChild(this.drawingGraphic)
	};
	//render graphics
	renderer.prototype.events = {};
	renderer.prototype.events.resize = function () {
	    this.adjustBounds();
	    this.pixi.render(this.stage);
	};
	//set bounds for graphics
	renderer.prototype.adjustBounds = function () {
	    var outerW = this.elementContainer.offsetWidth
	    var outerH = outerW / 3 * 2

	    this.viewport.width = outerW
	    this.viewport.height = outerH
	    this.viewport.scale = outerW / 1200 * 35

	    this.offset = this.pixi.view.getBoundingClientRect()
	    this.offset = {
	        top: this.offset.top + document.body.scrollTop,
	        left: this.offset.left + document.body.scrollLeft
	    }

	    this.pixi.resize(this.viewport.width, this.viewport.height)
	 };
	 //render all graphics shapes 
	renderer.prototype.render = function () {
	    for (var i = 0; i < this.bodies.length; i++) {
	        this.update_body(this.bodies[i]);
	    }

	    this.update_stage(this.stage);
	    this.pixi.render(this.stage);
	};
	//update graphics shapes and body
	renderer.prototype.update_stage = function (stage) {
	    stage.scale.x = this.viewport.scale;
	    stage.scale.y = this.viewport.scale;
	    stage.position.x = this.viewport.center[0] + this.viewport.width / 2;
	    stage.position.y = this.viewport.center[1] + this.viewport.height / 2;
	};

	renderer.prototype.update_body = function (body) {
	    body.gl_sprite.position.x = body.interpolatedPosition[0]
	    body.gl_sprite.position.y = body.interpolatedPosition[1]
	    body.gl_sprite.rotation = body.interpolatedAngle
	};
	//generate sprites for shapes
	renderer.prototype.create_sprite = function (body) {

	    var sprite = new PIXI.Graphics();

	    this.draw_sprite(body, sprite);
	    this.stage.addChild(sprite);

	    if (body.gl_create) {
	        body.gl_create(sprite, this);
	    }

	    return sprite;
	};
	//draw shapes
	renderer.prototype.draw_path = function (sprite, path, opt) {
	    if (path.length < 2) 
	        return ;

	    if (typeof opt.line !== 'undefined') {
	        sprite.lineStyle(opt.line.width, opt.line.color, opt.line.alpha);
	    }
	    
	    if (typeof opt.fill !== 'undefined') {
	        sprite.beginFill(opt.fill.color, opt.fill.alpha);
	    }
	    
	    sprite.moveTo(path[0][0], path[0][1]);
	    for (var i = 1; i < path.length; i++) {
	        var p = path[i];
	        sprite.lineTo(p[0], p[1]);
	    }

	    if (opt.fill !== 'undefined') {
	        sprite.endFill();
	    }
	}

	renderer.prototype.draw_rect = function (sprite, bounds, angle, opt) {
	    var w = bounds.w, h = bounds.h, x = bounds.x, y = bounds.y;
	    var path = [
	        [w / 2, h / 2],
	        [-w / 2, h / 2],
	        [-w / 2, -h / 2],
	        [w / 2, -h / 2],
	        [w / 2, h / 2]
	    ];

	    // Rotate and add position
	    for (var i = 0; i < path.length; i++) {
	        var v = path[i];
	        p2.vec2.rotate(v, v, angle);
	        p2.vec2.add(v, v, [x, y]);
	    }

	    this.draw_path(sprite, path, opt);
	}

	renderer.prototype.draw_sprite = function (body, sprite) {
	    sprite.clear();

	    var color = body.color
	    var opt = {
	        line: { color: color, alpha: 1, width: 0.01 },
	        fill: { color: color, alpha: 1.0 }
	    }

	    if(body.concavePath){
	        var path = []

	        for(var j=0; j!==body.concavePath.length; j++){
	            var v = body.concavePath[j]
	            path.push([v[0], v[1]])
	        }

	        this.draw_path(sprite, path, opt)

	        return 
	    }

	    for (var i = 0; i < body.shapes.length; i++) {
	        var shape = body.shapes[i],
	            offset = shape.position,
	            angle = shape.angle;

	        var shape_opt = opt
	        if (shape.color) {
	            shape_opt = {
	                line: { color: shape.color, alpha: 1, width: 0.01 },
	                fill: { color: shape.color, alpha: 1.0 }
	            }
	        }

	        if (shape instanceof p2.Box) {
	            this.draw_rect(sprite, { w: shape.width, h: shape.height, x: offset[0], y: offset[1] }, angle, shape_opt);
	        }

	        else if (shape instanceof p2.Convex) {
	            var path = [], v = p2.vec2.create();
	            for(var j = 0; j < shape.vertices.length; j++){
	                p2.vec2.rotate(v, shape.vertices[j], angle);
	                path.push([v[0]+offset[0], v[1]+offset[1]]);
	            }

	            this.draw_path(sprite, path, shape_opt);
	        }
	    }
	};  

	renderer.prototype.add_body = function (body) {
	    if (body instanceof p2.Body && body.shapes.length && !body.hidden) {
	        body.gl_sprite = this.create_sprite(body);
	        this.update_body(body);
	        this.bodies.push(body);
	    }
	};

	renderer.prototype.remove_body = function (body) {
	    if (body.gl_sprite) {
	        this.stage.removeChild(body.gl_sprite)

	        for (var i = this.bodies.length; --i; ) {
	            if (this.bodies[i] === body) {
	                this.bodies.splice(i, 1);
	            }
	        }
	    }
	};

	renderer.prototype.zoom = function (factor) {
	    this.viewport.scale *= factor;
	};

	var sampling = 0.4
	renderer.prototype.mousedown = function (pos) {
	    this.drawPoints = [ [ pos.x, pos.y ] ]
	};

	renderer.prototype.mousemove = function (pos) {
	    pos = [ pos.x, pos.y ]

	    var sqdist = p2.vec2.distance(pos,this.drawPoints[this.drawPoints.length-1]);
	    if (sqdist > sampling*sampling){
	        this.drawPoints.push(pos)

	        this.drawingGraphic.clear()
	        this.draw_path(this.drawingGraphic, this.drawPoints, {
	            line: {
	                width: 0.02,
	                color: 0xFF0000,
	                alpha: 0.9
	            }
	        })
	    }
	}

	renderer.prototype.mouseup = function (pos) {
	    if (this.drawPoints.length > 2) {    
	        this.world.addBodyFromPoints(this.drawPoints)
	    }

	    this.drawPoints = []
	    this.drawingGraphic.clear()
	};


	module.exports = renderer;

 },
//--------------------------------------------------------------------------------------------------
//Implements agent and car using gravitational forces with P2.js library
 function(module, exports, getFunctions) {

	var agent = getFunctions(2)
	var color = getFunctions(4)
	var car = getFunctions(3)

	function world() {
		//set gravity on P2.js
	    this.agents = [];
	    this.p2 = new p2.World({
	        gravity : [0,0]
	    });
	    //set Gravitational force on P2.js
	    this.p2.solver.tolerance = 0
	    this.p2.solver.iterations = 50
	    this.p2.setGlobalStiffness(1e8)
	    this.p2.setGlobalRelaxation(1)

	    this.age = 0.0
	    this.timer = 0
	    this.smoothReward = 0

	    this.plotRewardOnly = false

	    this.obstacles = []
	    //add data of sensors, actions, input, states to Neuro.JS
	    var state = car.Sensors.dimensions, actions = 2, input = 2 * state + 1 * actions
	    this.brains = {
	    	//set network model
	        actor: new window.neurojs.Network.Model([

	            { type: 'input', size: input }, //data input sensors
	            { type: 'fc', size: 60, activation: 'relu' }, //neural activations
	            { type: 'fc', size: 40, activation: 'relu', dropout: 0.30 }, //neural activations
	            { type: 'fc', size: actions, activation: 'tanh' }, //neural activations
	            { type: 'regression' }

	        ]),

	        //for DDPG algorithm, we use critic states
	        critic: new window.neurojs.Network.Model([

	            { type: 'input', size: input + actions }, //input data from sensors
	            { type: 'fc', size: 80, activation: 'relu' },  //neural activations
	            { type: 'fc', size: 70, activation: 'relu' },  //neural activations
	            { type: 'fc', size: 60, activation: 'relu' },  //neural activations
	            { type: 'fc', size: 50, activation: 'relu' },  //neural activations
	            { type: 'fc', size: 1 },
	            { type: 'regression' } //training type [regression]

	        ])

	    }
	    //set values to Neuro.JS and implement
	    this.brains.shared = new window.neurojs.Shared.ConfigPool()

	    // this.brains.shared.set('actor', this.brains.actor.newConfiguration())
	    this.brains.shared.set('critic', this.brains.critic.newConfiguration())
	};	
	//add body to world


	
	//initialize obstacles
	world.prototype.addObstacle = function (obstacle) {
	    this.p2.addBody(obstacle)
	    this.obstacles.push(obstacle)
	    console.log(obstacles);
	};
	//initialize walls
	world.prototype.addWall = function (start, end, width) {
	    var w = 0, h = 0, pos = []
	    if (start[0] === end[0]) { // hor
	        h = end[1] - start[1];
	        w = width
	        pos = [ start[0], start[1] + 0.5 * h ]
	    }
	    else if (start[1] === end[1]) { // ver
	        w = end[0] - start[0]
	        h = width
	        pos = [ start[0] + 0.5 * w, start[1] ]
	    }
	    else 
	        throw 'error'

	    // Create box
	    var b = new p2.Body({
	        mass : 0.0,
	        position : pos
	    });
	    //create rectangle Shapes
	    var rectangleShape = new p2.Box({ width: w, height:  h });
	    // rectangleShape.color = 0xFFFFFF
	    b.hidden = true;
	    b.addShape(rectangleShape);
	    this.p2.addBody(b);

	    return b;
	}
	//add shape on graphics
	world.prototype.addShape = function (start, end, width,height) {
	    // Create box
	    var b = new p2.Body({
	        mass : 0.0,
	        position : [start,end]
	    });

	    var rectangleShape = new p2.Box({ width: width, height:  height });
	     rectangleShape.color = 0xbfbfbf;
	   // b.hidden = true;
	    b.addShape(rectangleShape);
	    this.p2.addBody(b);

	    return b;
	}
	//initialize graphics on main
	world.prototype.init = function (renderer) {
	    window.addEventListener('resize', this.resize.bind(this, renderer), false);

	    var w = renderer.viewport.width / renderer.viewport.scale
	    var h = renderer.viewport.height / renderer.viewport.scale
	    var wx = w / 2, hx = h / 2
	    //adding wall to not car get out of graphics
	    this.addWall( [ -wx - 0.25, -hx ], [ -wx - 0.25, hx ], 0.5 )
	    this.addWall( [ wx + 0.25, -hx ], [ wx + 0.25, hx ], 0.5 )
	    this.addWall( [ -wx, -hx - 0.25 ], [ wx, -hx - 0.25 ], 0.5 )
	    this.addWall( [ -wx, hx + 0.25 ], [ wx, hx + 0.25 ], 0.5 )
	    //adding shapes on graphics
		this.addShape(  15,-3, 0.5,12 )
	    this.addShape(  12.5,-12, 0.5,6 )
	    this.addShape(  10.74,-9, 4,0.5 )
	    this.addShape(  -4,-9, 22,0.5 )
	    this.addShape(  -4,-9.6, 0.5,1 )
	    this.addShape(  -12,-11.3, 0.5,2 )
	    this.addShape(  3,-11.3, 0.5,2 )
	    this.addShape(  13,7, 4,4 )
	    this.addShape(  -15,1.5, 0.5,15 )
	    this.addShape(  -7.5,-5.76, 15,0.5 )
	    this.addShape(  -7.5,8.76, 15,0.5 )
	    this.addShape(  -0.27,3.5, 0.5,10 )
	    this.addShape(  -0.27,-5.7, 0.5,3 )
	    this.addShape(  -6,1.5, 6,6 )
	    this.addShape(  -12,1.5, 0.5,6 )
	    this.addShape(  -12.7,1.5, 1,0.5 )
	    this.addShape(  -6,-2, 2,2 )
	    this.addShape(  -6,5, 2,2 )
		this.addShape(  -6,6.5, 0.5,1 )
		this.addShape(  3,5, 2,2 )
		this.addShape(  3,1, 2,2 )
		this.addShape(  3,-3, 2,2 )
		this.addShape(  8,-3, 2,2 )
		this.addShape(  8,3, 2,2 )
		this.addShape(  12,-1, 2,2 )
	    this.size = { w, h }
	};
	//populate agent on graphics
	world.prototype.populate = function (n) {
	    var ag = new agent({}, this);
	        this.agents.push(ag);
	};
	//resize for bootstrap
	world.prototype.resize = function (renderer) {
	};
		//steps and Reward function
	world.prototype.step = function (dt) {
	    if (dt >= 0.02)  dt = 0.02;

	    ++this.timer

	    var loss = 0.0, reward = 0.0, agentUpdate = false
	 
	    	//update agent, reward and punish
	        agentUpdate = this.agents[0].step(dt);
	        loss += this.agents[0].loss //punish agent
	        reward += this.agents[0].reward  //reward agent
			// document.getElementById('recompensa').innerHTML=Math.floor(reward);
			var recompensa = document.getElementById('recompensa-indicator');
			//show updates of reward data on map
			if(Math.floor(reward)<=0){
				recompensa.src = "assets/recompensa/0.png";
			}
			else if(Math.floor(reward)>0 && Math.floor(reward)<=10){
				recompensa.src = "assets/recompensa/1.png";
			}
			else if(Math.floor(reward)>=10 && Math.floor(reward)<=20){
				recompensa.src = "assets/recompensa/2.png";
			}
			else if(Math.floor(reward)>=20 && Math.floor(reward)<=30){
				recompensa.src = "assets/recompensa/3.png";
			}
			else if(Math.floor(reward)>=40 && Math.floor(reward)<=50){
				recompensa.src = "assets/recompensa/5.png";
			}
			else if(Math.floor(reward)>=60 && Math.floor(reward)<=70){
				recompensa.src = "assets/recompensa/6.png";
			}
			else if(Math.floor(reward)>=70 && Math.floor(reward)<=80){
				recompensa.src = "assets/recompensa/7.png";
			}
			else if(Math.floor(reward)>=80 && Math.floor(reward)<=90){
				recompensa.src = "assets/recompensa/8.png";
			}
			else if(Math.floor(reward)>=100 && Math.floor(reward)<=110){
				recompensa.src = "assets/recompensa/9.png";
			}
			else if(Math.floor(reward)>=110 && Math.floor(reward)<=120){
				recompensa.src = "assets/recompensa/10.png";
			}
			else if(Math.floor(reward)>=120 && Math.floor(reward)<=130){
				recompensa.src = "assets/recompensa/11.png";
			}
			else if(Math.floor(reward)>=130 && Math.floor(reward)<=140){
				recompensa.src = "assets/recompensa/12.png";
			}
			else if(Math.floor(reward)>=140 && Math.floor(reward)<=150){
				recompensa.src = "assets/recompensa/13.png";
			}
			else if(Math.floor(reward)>=150 && Math.floor(reward)<=160){
				recompensa.src = "assets/recompensa/14.png";
			}
			else if(Math.floor(reward)>=160 && Math.floor(reward)<=170){
				recompensa.src = "assets/recompensa/15.png";
			}

	   
		//neurojs steps
	    this.brains.shared.step()
	    this.p2.step(1 / 60, dt, 10);
	    this.age += dt
	};
	//initialize obstacles Pixi.js
	world.prototype.export = function () {
	    var contents = []
	    contents.push({
	        obstacles: this.obstacles.length
	    })



	    for (var i = 0; i < this.obstacles.length; i++) {
	        contents.push(this.obstacles[i].outline)
	    }


	    //initialize body of agent and read contents of Neuro.js
	    var agent = []
	      agents.push({
	            location: this.agent[0].car.chassisBody.position,
	            angle: this.agent[0].car.chassisBody.angle
	        })
	    contents.push(agent)

	    return window.neurojs.Binary.Writer.write(contents)
	};
	//update obstacles if there is a collission, change car body
	world.prototype.clearObstacles = function () {
	    for (var i = 0; i < this.obstacles.length; i++) {
	        this.p2.removeBody(this.obstacles[i])
	    }

	    this.obstacles = []
	};
	//export functions that initialize agent, data, and obstacles
	world.prototype.import = function (buf) {
	    this.clearObstacles()

	    var contents = window.neurojs.Binary.Reader.read(buf)
	    var j = -1
	    var meta = contents[++j]

	    for (var i = 0; i < meta.obstacles; i++) {
	        this.addBodyFromCompressedPoints(contents[++j])
	    }
	    //agent's content
	    var agent = contents[++j]
	    //set agent's car chassis on map based on random location, position and angle using Pixi.js
	    if (agent.length !== this.agent.length) {
	        throw 'error';
	    }
	    this.agents[0].car.chassisBody.position = agents[i].location
	        this.agents[0].car.chassisBody.angle = agents[i].angle
	};

	module.exports = world;

}
]);