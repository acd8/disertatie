Self driving car

Introduction

The mission is to develop an sustainable agent that can avoid obstacles, learn data from user�s input and after this,  agent can be rewarded due to safe actions taken on the environment. Most part of this project include supervised learning because learning data from user�s input in real-time based on user�s actions can take less time and more high accuracy. Agent's sensors include distance sensor, collision sensor, speed sensor. Sensors is the most important part because the main purpose is to avoid walls and obstacles in the road with high seed. Algorithm used in this project is DDPG (Deep Deterministic Policy Gradient) also known Deep Reinforcement Learning that uses lots of tricks from Deep Learning algorithms. Autopilot is rewarded after completed successfully tasks that avoid obstacles in a normal or high speed. That is the main objective of this project, to avoid obstacles in a good manner and of course to maximize the reward.

How to run this project

    1.Download or clone project.
    2.Open index.html.
    3.For supervised learning, press key "M" and control the car with keyboard arrows (up, down, left, right).
    4.For unsupervised learning, press key "A" and wait until car learn autonomously to drive (This can take 1-2 hours)
    5.If you want to save or load data, just press save/load buttons.

Libraries used

    1.Pixi.js
    2.P2.js
    3.NeuroJs
    4.JQuery& JQuery UI
    5.Bootstrap
